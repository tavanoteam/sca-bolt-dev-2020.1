/*
	© 2020 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/// <amd-module name="Loggers.Appender.Sensors"/>
/// <reference path="../../../Commons/Utilities/JavaScript/GlobalDeclarations.d.ts" />

import * as _ from 'underscore';
import { LoggersAppender } from '../../../Commons/Loggers/JavaScript/Loggers.Appender';

import { loggersAppenderSensorsSiteData } from './Loggers.Appender.Sensors.SiteData';
import { loggersAppenderSensorsBundle } from './Loggers.Appender.Sensors.Bundle';
import { loggersAppenderSensorsCart } from './Loggers.Appender.Sensors.Cart';
import { loggersAppenderSensorsShopper } from './Loggers.Appender.Sensors.Shopper';
import { loggersAppenderSensorsCustomer } from './Loggers.Appender.Sensors.Customer';
import { loggersAppenderSensorsDevice } from './Loggers.Appender.Sensors.Device';
import { loggersAppenderSensorsErrorStatus } from './Loggers.Appender.Sensors.ErrorStatus';
import { ItemTrack } from '../../../Commons/Instrumentation/JavaScript/APMTrackerParameters';

import * as jQuery from '../../../Commons/Core/JavaScript/jQuery';

interface LoggersOptions {
    baseLabel: string;
    buildNo: string;
    bundleId: string;
    bundleName: string;
    bundleVersion: string;
    currencyCode: string;
    customerSessionStatus: string;
    dateLabel: string;
    device: string;
    shopperInternalId: string;
    siteUrl: string;
    visitorId: string;
}

export interface NavigationLoggerOptionalsOptions {
    searchQuery: string;
    searchResultCount: number;
    searchPageNumber: number;
    itemId: number;
}

interface StandardLoggerOptionsOptionals extends Partial<ItemTrack> {
    operationIds: string[];
    status: string;
    transactionId?: number;
}

interface NavigationLoggersOptions
    extends LoggersOptions,
        Partial<NavigationLoggerOptionalsOptions> {
    errorStatus: undefined | string;
    siteFragment: string;
    sitePage: string;
    sitePageDisplayName: string;
    showContentTime: number;
}

interface StandardLoggersOptions extends LoggersOptions, StandardLoggerOptionsOptionals {
    actionId: string;
    cartLines: string;
}

type LoggerEndOptions = Partial<NavigationLoggerOptionalsOptions> &
    Partial<StandardLoggerOptionsOptionals>;

export class LoggersAppenderSensors implements LoggersAppender {
    private static instance: LoggersAppenderSensors;

    private dataExtractorNavigation = [
        loggersAppenderSensorsSiteData,
        loggersAppenderSensorsBundle,
        loggersAppenderSensorsCustomer,
        loggersAppenderSensorsShopper,
        loggersAppenderSensorsDevice,
        loggersAppenderSensorsErrorStatus
    ];

    private dataExtractor = [
        loggersAppenderSensorsSiteData,
        loggersAppenderSensorsBundle,
        loggersAppenderSensorsCustomer,
        loggersAppenderSensorsShopper,
        loggersAppenderSensorsDevice
    ];

    private NLRUM: any;

    private firstTime: boolean = true;

    private applicationStartTime: number;

    private paramsMap = {};

    private enabled = !SC.isPageGenerator();

    private registerMap() {
        this.paramsMap = {
            status: {
                key: 'status',
                values: {
                    success: this.NLRUM.status.completed,
                    fail: this.NLRUM.status.incomplete,
                    cancelled: this.NLRUM.status.cancelled
                }
            }
        };
    }

    private mapParams(params: StandardLoggersOptions) {
        const mapped = {};

        _.mapObject(params, (value, key) => {
            if (this.paramsMap[key]) {
                mapped[this.paramsMap[key].key] =
                    this.paramsMap[key].values && this.paramsMap[key].values[value]
                        ? this.paramsMap[key].values[value]
                        : value;
            } else {
                mapped[key] = value;
            }
        });

        return mapped;
    }

    protected constructor() {
        // don't execute in Page Generator
        if (this.enabled) {
            this.NLRUM = (<any>window).NLRUM;

            if (this.NLRUM && this.NLRUM.addSCData) {
                this.applicationStartTime = (<any>window).applicationStartTime;
                this.NLRUM.setCommerceContext(this.NLRUM.commerceContext.SCA);
                this.registerMap();
            } else {
                this.enabled = false;
                console.log('nlRUM.js failed to load');
            }
        }
    }

    ready() {
        return this.enabled;
    }

    info() {}

    error() {}

    private startNavigation() {
        if (this.applicationStartTime) {
            const options: any = { action: 'Navigation' };
            if (this.firstTime) {
                this.firstTime = false;
                options.startTime = this.applicationStartTime;
            } else {
                this.NLRUM.markIndirectStart && this.NLRUM.markIndirectStart();
                options.startTime = Date.now();
            }
            return options;
        }
    }

    public start(action: string, _options: object) {
        if (action === 'Navigation') {
            return this.startNavigation();
        }
        const promise = jQuery.Deferred();
        const actionId = this.NLRUM.actionStart(action);
        loggersAppenderSensorsCart.extract().done(cart => {
            promise.resolve({
                cartLines: cart.cartLines,
                actionId: actionId
            });
        });
        return { promise: promise };
    }

    private endNavigation(options) {
        if (this.applicationStartTime) {
            const self = this;
            const data: NavigationLoggersOptions = {
                baseLabel: '',
                buildNo: '',
                bundleId: '',
                bundleName: '',
                bundleVersion: '',
                currencyCode: '',
                customerSessionStatus: '',
                dateLabel: '',
                device: '',
                shopperInternalId: '',
                siteUrl: '',
                visitorId: '',
                siteFragment: '',
                sitePage: '',
                sitePageDisplayName: '',
                errorStatus: undefined,
                showContentTime: 0
            };
            if (options.itemId) {
                data.itemId = options.itemId;
            }
            if (options.searchQuery) {
                data.searchQuery = options.searchQuery;
            }
            if (options.searchResultCount !== undefined) {
                data.searchResultCount = options.searchResultCount;
            }
            if (options.searchPageNumber !== undefined) {
                data.searchPageNumber = options.searchPageNumber;
            }

            data.showContentTime = Date.now() - options.startTime;
            jQuery.when
                .apply(jQuery, _.invoke(this.dataExtractorNavigation, 'extract'))
                .done((...results) => {
                    self.NLRUM.addSCData(
                        LoggersAppenderSensors.loggerData(results, data, 'Navigation')
                    );
                });
        }
    }

    private static loggerData(
        loggerOptionsItem: LoggersOptions[],
        data: LoggersOptions,
        loggerType: string = 'Not Navigation'
    ): LoggersOptions {
        const navigationProperties = ['sitePage', 'siteFragment', 'sitePageDisplayName'];

        _.each(loggerOptionsItem, argument => {
            Object.getOwnPropertyNames(argument).forEach((key: string) => {
                let addItem = false;
                if (loggerType === 'Navigation' || navigationProperties.indexOf(key) === -1) {
                    addItem = true;
                }
                if (addItem) {
                    data[key] = argument[key];
                }
            });
        });
        return data;
    }

    private extractOperationIds(options) {
        if (_.isString(options.operationIds)) {
            return [options.operationIds];
        }
        if (_.isArray(options.operationIds)) {
            return options.operationIds.slice(0);
        }
        return [];
    }

    end(dataStart, options?: LoggerEndOptions) {
        if (dataStart.action === 'Navigation') {
            dataStart = _.extend(dataStart, options);
            this.endNavigation(dataStart);
        } else {
            dataStart.promise.done(data => {
                const trackOptions: StandardLoggersOptions = {
                    actionId: '',
                    baseLabel: '',
                    buildNo: '',
                    bundleId: '',
                    bundleName: '',
                    bundleVersion: '',
                    cartLines: '',
                    currencyCode: '',
                    customerSessionStatus: '',
                    dateLabel: '',
                    device: '',
                    operationIds: [],
                    shopperInternalId: '',
                    siteUrl: '',
                    status: '',
                    visitorId: ''
                };
                if (options.itemId) {
                    trackOptions.itemId = options.itemId;
                }
                if (options.itemQuantity) {
                    trackOptions.itemQuantity = options.itemQuantity;
                }
                if (options.transactionId) {
                    trackOptions.transactionId = options.transactionId;
                }
                trackOptions.operationIds = this.extractOperationIds(options);
                trackOptions.actionId = data.actionId;
                trackOptions.status = options.status;
                trackOptions.cartLines = data.cartLines;
                jQuery.when
                    .apply(jQuery, _.invoke(this.dataExtractor, 'extract'))
                    .done((...results) => {
                        this.NLRUM.actionEnd(
                            data.actionId,
                            this.mapParams(
                                _.extend(
                                    {},
                                    data,
                                    LoggersAppenderSensors.loggerData(results, trackOptions)
                                )
                            )
                        );
                    });
            });
        }
    }

    static getInstance() {
        if (!LoggersAppenderSensors.instance) {
            LoggersAppenderSensors.instance = new LoggersAppenderSensors();
        }

        return LoggersAppenderSensors.instance;
    }
}
