/*
	© 2020 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/// <amd-module name="Loggers.Appender.Sensors.Cart"/>

import * as jQuery from '../../../Commons/Core/JavaScript/jQuery';

import ComponentContainer = require('../../../Commons/SC/JavaScript/SC.ComponentContainer');

const loggersAppenderSensorsCart = {
    extract: () => {
        const cartPromise = jQuery.Deferred();
        const cart = ComponentContainer.getComponent('Cart');

        cart.getLines().done(lines => {
            const data = {
                cartLines: lines.length + ''
            };

            cartPromise.resolve(data);
        });

        return cartPromise;
    }
};

export { loggersAppenderSensorsCart };
