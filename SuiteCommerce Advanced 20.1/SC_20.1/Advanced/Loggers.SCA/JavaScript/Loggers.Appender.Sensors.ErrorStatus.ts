/*
	© 2020 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/// <amd-module name="Loggers.Appender.Sensors.ErrorStatus"/>

import * as jQuery from '../../../Commons/Core/JavaScript/jQuery';

import ComponentContainer = require('../../../Commons/SC/JavaScript/SC.ComponentContainer');

const loggersAppenderSensorsErrorStatus = {
    extract: () => {
        const { application } = ComponentContainer.getComponent('Layout');

        const current_view = application.getLayout().getCurrentView();
        const errorData =
            current_view.isErrorView && (current_view.getPageDescription() || 'error');

        return jQuery.Deferred().resolve({ errorStatus: errorData });
    }
};

export { loggersAppenderSensorsErrorStatus };
