/*
	© 2020 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/// <amd-module name="Loggers.Appender.Sensors.SiteData"/>

import * as jQuery from '../../../Commons/Core/JavaScript/jQuery';

import ComponentContainer = require('../../../Commons/SC/JavaScript/SC.ComponentContainer');

const loggersAppenderSensorsSiteData = {
    extract: () => {
        const pageType = ComponentContainer.getComponent('PageType');
        const context = pageType.getContext();

        const data = {
            sitePage: context.page_type,
            siteFragment: context.path,
            sitePageDisplayName: context.page_type_display_name,
            siteUrl: SC.ENVIRONMENT.shoppingDomain
        };

        return jQuery.Deferred().resolve(data);
    }
};

export { loggersAppenderSensorsSiteData };
